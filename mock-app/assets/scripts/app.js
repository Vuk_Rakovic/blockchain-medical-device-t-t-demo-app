angular.module("App", [])
.controller('testCtrl', function($scope, $http, $timeout){

    var address = 'http://localhost:3000';
    $scope.mock = null;
    $scope.message = "Waiting..."
    $scope.nodevices = 0;
    $scope.nolocations = 0;
    $scope.devicefails = 0;
    $scope.locationfails = 0;

    var bootstrapData = function() {               
        for(var i = 0; i < $scope.mock.participants.length; i++) {
            $scope.mock.participants[i].id = "l"+(i+1);
            $http.post(address + '/api/Location', $scope.mock.participants[i], null).then(function() { 
                if($scope.nolocations + $scope.locationfails == 4) { 
                    postAssets() 
                }; 
                $scope.nolocations++;
            }, 
            function () {
                if($scope.nolocations + $scope.locationfails == 4) { 
                    postAssets() 
                }; 
                $scope.locationfails++;
                $scope.message = 'Unsuccessful';
            });
        }            
    }

    var postAssets = function() {
        for(var j = 0; j < $scope.mock.assets.length; j++) {
            $scope.mock.assets[j].location = "l1";
            $scope.mock.assets[j].deviceState = $scope.mock.participants[0].deviceInLocationState;
            $scope.mock.assets[j].deviceStatus = "AVAILABLE";
            $scope.mock.assets[j].locationName = $scope.mock.participants[0].name;
            $http.post(address + '/api/Device', $scope.mock.assets[j], null).then(function() {
                $scope.nodevices++ 
                if($scope.nodevices == $scope.mock.assets.length && $scope.message != 'Unsuccessful') {
                    $scope.message = 'Successful';
                }
            },
            function(response) {
                $scope.devicefails++;
                $scope.message = 'Unsuccessful';
            });
        }         
    }

    function init() { 
        $http.get('mockdata.json').
        then(function onSuccess(response) {
            $scope.mock = angular.extend({}, response.data);
            $scope.participants = $scope.mock.participants
            $scope.assets = $scope.mock.assets
            $scope.assetsCount = $scope.assets.length;
            $scope.participantsCount = $scope.participants.length;
            bootstrapData();
        }).
        catch(function onError(response) {
         console.log(response);
        });
    }
    init();
})
