var app = angular.module('NT.App');
app.factory('dataFactory', function() {

    var factory = {};
    
        factory.fillData = function(device) {
        //rules for data representation - adjust how data will be represented
        
        var productCodes = {};
        for(var i = 0;i < device.productCodes.length; i++) {
            productCodes[device.productCodes[i].productCode] = device.productCodes[i].productCodeName;
        }
    
        var transactions = [];
        
        if(device.hasOwnProperty('deviceTransactions')) {
            for (var k = 0; k < device.deviceTransactions.length; k++) {
                var transaction = processTransaction(device.deviceTransactions[k])
                transactions.push(transaction);
            }
        }
    
        
        var data = 
            [
                {
                    "Device identification" : 
                    {
                        "UDI":device.udi,
                        "Location" : device.locationName,
                        "State" : device.deviceState,
                        "Status" : device.deviceStatus,
                        "Production Identifier":device.productionIdentifier,
                        "Device Identifier":device.deviceIdentifier,
                        "Part of":device.partOf,
                        "Company name":device.companyName,
                        "Brand Name":device.brandName,
                        "Description":device.deviceDescription,
                        "Issuing Agency":device.issuingAgency,
                        "Catalog Number":device.catalogNumber,
                        "Device Count":device.deviceCount,
                        "Device Class:":device.deviceClass
                    },
                    "Device production identifiers" : 
                    {
                        "Lot or Match number":device.lotBatch,
                        "Serial Number":device.serialNumber,
                        "Manufacturing Date":device.manufacturingDate,
                        "Expiration Date":device.expirationDate
                    },
                    "Device status" : 
                    {
                        "DI Record Status":device.deviceRecordStatus,
                        "DI Record Publish Date":device.devicePublishDate,
                        "Commercial Distribution End Date":device.deviceCommDistributionEndDate,
                        "Commercial Distribution Status":device.deviceCommDistributionStatus
                    }
                },
                {   
                    "Device Characteristics" : 
                    {
                        "For Single Use":device.singleUse,
                        "Device Packed as Sterile":device.deviceSterile,
                        "Requires Sterilisation Prior To Use":device.sterilizationPriorToUse,
                        "Sterilisation Methods":device.methodTypes
                    },
                    "Product Code" : productCodes
                },
                {
                    "Transactions" : transactions
                }
            ]

        return data;
    }
    
    var processTransaction = function(tx) {
        var transaction = {};  
        var transport = tx.$class.endsWith("Transport");          
        transaction["Transaction"] = transport ? "DeviceTransport" : "DeviceRejection";
        transaction["Device udi"] = tx.device.substr(tx.device.length - 56);
        if (transport) {
            transaction["New location"] = tx.newLocationName;
        }
        transaction["Device state"] = tx.deviceState;
        transaction["Description"] = tx.description;
        if (!transport) {
            transaction["Reason"] = tx.rejectionReason;
        }
        var d = new Date();
        transaction["Timestamp"] = tx.timestamp;
        return transaction;
    }
    
    return factory;

});