var app = angular.module('NT.App');
app.factory('initFactory', function($http) {
    var factory = {};
    
    var ipaddress = 'http://localhost:3000';
    
    var locations = null;

    $http.get(ipaddress + '/api/Location/', null).then(function(response) {             
        locations = response.data;
    }); 

    var deviceTransportButtonLabels = ["Send to Port Authority","Send to Shippment","Send to Warehouse","Send to Hospital"]
    var deviceRejectButtonLabels = ["Scrap","Reject","Suspend","Dismiss","Cancel order"]          
    var defaultTransportLabel = "Transport";
    var defaultRejectedLabel = "Rejected";
    

    factory.getIpAddress = function() {return ipaddress; }

    factory.getDeviceTransportButtonLabel = function(i) {
        if(deviceTransportButtonLabels == null || i >= deviceTransportButtonLabels.length) {
            return defaultTransportLabel;
        }
        return deviceTransportButtonLabels[i];
    } 
    
    factory.getDeviceRejectButtonLabel = function(i) {
        if(deviceRejectButtonLabels == null || i >= deviceRejectButtonLabels.length) {
            return defaultRejectedLabel;
        }
        return deviceRejectButtonLabels[i];
    }

    factory.getLocation = function(i) {
        return locations[i-1];     
    }

    factory.getLastLocationId = function() {
        return locations[locations.length-1].id
    }


    return factory;
})
