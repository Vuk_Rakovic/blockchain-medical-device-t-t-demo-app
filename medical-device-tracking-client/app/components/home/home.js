﻿
//'use strict';

var app = angular.module('NT.App');

app.controller('HomeCtrl', function ($scope, $state, $http, initFactory, dataFactory, txAnimationFactory) {

    var vm = this;

    vm.handscannerProcessingEnabled = true;
    var ipaddress = initFactory.getIpAddress();
    vm.inputUdi = null;
    $scope.deviceDelivered = false;
    $scope.deviceAvailable = false;
    $scope.rejectionDescription = '';
    $scope.deviceNotFound = false;
    $scope.dataGroup = null;
    var currentDevice = null;
    $scope.devicestr = '';
    vm.oneAtATime = true;

    $scope.disableGif = function () {
        vm.handscannerProcessingEnabled = true;
    }

    $scope.changeState = function (stateName) {

        $state.go(stateName);
        if (stateName == 'home.details') {
            $scope.detailsActive = "active";
        } else {
            $scope.detailsActive = "";
        }

        if (stateName == 'home.characteristics') {
            $scope.detailsCharacteristics = "active";
        } else {
            $scope.detailsCharacteristics = "";
        }

        if (stateName == 'home.history') {

            $state.go('home.error');
            $scope.detailsHistory = "active";
        } else {
            $scope.detailsHistory = "";
        }
    }

    vm.executeOnEnter = function (keyEvent) {
        if (keyEvent.which === 13 && vm.handscannerProcessingEnabled && vm.inputUdi) {
            if (!(currentDevice != null && vm.inputUdi == currentDevice.udi)) {             
                getDevice(vm.inputUdi);
                $scope.changeState('home.details')
            }
        }
    };

    var getDevice = function(udi) {
        vm.handscannerProcessingEnabled = false;
        $http.get(ipaddress + '/api/Device/'+udi, null).then(function(response) {             
            processDevice(response.data);
            vm.handscannerProcessingEnabled = true;
            $scope.changeState('home.details');
        }, function (response) { 
            vm.handscannerProcessingEnabled = true;
            $scope.dataGroup = null; 
            $scope.deviceDelivered = false;
            $scope.deviceNotFound = true;
            document.getElementById("test").innerHTML = "";            
        });
    }    

    vm.applyOperation = function(reason) {
        vm.handscannerProcessingEnabled = false;
        var delivered = $scope.deviceDelivered;   
        var device = currentDevice;
        var location = initFactory.getLocation(getLocationNumber(device.location));
        var state =  location.deviceRejectedState;
        var currentLocationName = location.name;
        var description = "";
        if(delivered)
            description = "Order canceled by " + currentLocationName + ". Device sent back to " + initFactory.getLocation(1).name;
        else
            description = "Rejected at " + currentLocationName + ". Device changed state to " + state;

        var data = {
            "device": device.udi,
            "deviceState": state,
            "description" : description,
            "rejectionReason" : reason,
            "restartLocation" : delivered ? "l1" : null //flag+info
        }       
        $http.post(ipaddress + '/api/DeviceRejection', data, null).then(function(response) {
            getDevice(currentDevice.udi);
        }, 
        function () {
            vm.handscannerProcessingEnabled = true;
        });
    }

    vm.transport = function() {      
        vm.handscannerProcessingEnabled = false;
        var device = currentDevice;  
        var locationNumber = getLocationNumber(device.location);         
        
        var location = initFactory.getLocation(locationNumber);
        var currentLocationName = location.name;
           
        var newLocation = initFactory.getLocation(locationNumber+1);
        var newLocationId = newLocation.id;          
        var newLocationName = newLocation.name;             
        var deviceState = newLocation.deviceInLocationState;
                
        var data = {
            "device": device.udi,
            "newLocation": newLocationId,
            "newLocationName": newLocationName,
            "currentLocationName" : currentLocationName,
            "deviceState": deviceState,
            "description"  : "Device moved from '" + currentLocationName + "' to '" + newLocationName + "'."
        }
        $http.post(ipaddress + '/api/DeviceTransport ', data, null).then(function(response) {
            getDevice(currentDevice.udi);
        }, function () {
            vm.handscannerProcessingEnabled = true;
        });       
    }

    var processDevice = function(device) {
        var existing = currentDevice != null ? currentDevice.udi == device.udi : false;
        var oldDevice = currentDevice != null ? currentDevice : null;       
        currentDevice = device;         
        $scope.deviceAvailable = currentDevice.deviceStatus == 'AVAILABLE';    
        $scope.deviceDelivered = (currentDevice.location.slice(-2) == initFactory.getLastLocationId());               
        if(!$scope.deviceDelivered) 
            $scope.deviceTransportButtonLabel = initFactory.getDeviceTransportButtonLabel(getLocationNumber(currentDevice.location)-1);                      
        else 
            $scope.deviceTransportButtonLabel = null; 
        $scope.deviceRejectButtonLabel = initFactory.getDeviceRejectButtonLabel(getLocationNumber(currentDevice.location)-1);
        $scope.devicestr = JSON.stringify(currentDevice, null, 2).trim();       
        $scope.dataGroup = dataFactory.fillData(currentDevice);
        if (!existing) {             
            document.getElementById("test").innerHTML = "";
            txAnimationFactory.appendPreviousTransactions($scope.dataGroup[2].Transactions);
        } else {
            txAnimationFactory.appendTransaction($scope.dataGroup[2].Transactions[$scope.dataGroup[2].Transactions.length-1]);
        }            
        $scope.deviceNotFound = false;
    }

    var getLocationNumber = function(location) {
        return parseInt(location.slice(-1));
    }

});



