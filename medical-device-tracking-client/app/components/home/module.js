angular
.module('NT.App', ['ui.router', 'ui.bootstrap', 'ngAnimate'])
.config([
       '$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider,intitFactory) {
           $urlRouterProvider.otherwise('/home/details');

           var loginState = {
               name: 'login',
               url: "/login",
               templateUrl: "app/components/login/login.html",
           };

           var homeState = {
               name: 'home',
               url: "/home",
               templateUrl: "app/components/home/home.html"
           };

           var devDetails = {
               name: 'home.details',
               url: "/details",
               templateUrl: "app/components/home/details.html",
           };

           var devChars = {
               name: 'home.characteristics',
               url: "/characteristics",
               templateUrl: "app/components/home/characteristics.html",
           };

           var devHistory = {
               name: 'home.history',
               url: "/history",
               templateUrl: "app/components/home/history.html",
           };

           var errorState = {
               name: 'home.error',
               url: "/error",
               templateUrl: "app/components/home/error.html",
           };

        $stateProvider
            .state(loginState)
            .state(homeState)
            .state(devDetails)
            .state(devChars)
            .state(devHistory)
            .state(errorState);

    }
])