var app = angular.module('NT.App');
app.factory('txAnimationFactory', function() {
    
    //creates tr/td table elements from key-value pairs and delegates the created code to the function wich then creates styled table out of it 
    //and applies the code to the page
    
    var factory = {};

    factory.appendTransaction = function(transaction) {
        appendTransaction(transaction);
    }

    var appendTransaction = function(transaction) {
        var tableElements = '';
        for(var key in transaction) {
            if (transaction.hasOwnProperty(key)) {
                var val = transaction[key];
                tableElements += "<tr>\r\n    <th>"+ key + ": <\/th>\r\n    <td>"+ val +"<\/td>\r\n  <\/tr>\r\n";
              }
        }
        transactionLog(tableElements);
    }
    
    factory.appendPreviousTransactions = function(transactions) {
        for (var i = 0; i < transactions.length; i++) {            
            appendTransaction(transactions[i])
        }              
    }
    
    var count = 1;
    var transactionLog = function (appendcode) {
                 
        if (count == 1) {
            var tableHeight = $(".myTable").height() + 20;
            $('#test').append('<div id="t' + count + '" style="width:97%;  border: 3px solid darkgrey; position:absolute; margin-bottom:12px;"><table class="table myTable" style=\"width:100%\">\r\n  '+ appendcode +'  <\/table></div>');
            $('#t' + count + '').css("right", "500px");
            $('#t' + count + '').css("opacity", "0.1");
                        
            var res = count * tableHeight;
            var resTop = res - tableHeight;
            
            $('#t' + count + '').animate({ right: '10px', height: tableHeight - 14, opacity: '1' });
            $('#t' + count + '').css('margin-top', '10px');
            $('#test').animate({ scrollBottom: $('#test').prop("scrollHeight") }, 500);
            tableElements = '';
        } else {
            var tableHeight = $(".myTable").height() + 20;
            var divHeight = tableHeight + 21;           
            var res = count * tableHeight;
            var prevCount = count - 1;
    
            for (var i = 1; i <= prevCount; i++) {
                resTop = res - tableHeight;
                $('#t' + i + '').animate({ top: '' + resTop + 'px', height: tableHeight - 14 }, 500);
                $('#t' + i + '').css('margin-top', '10px');
                res = (count - i) * tableHeight;
            }
                
            $('#test').append('<div id="t' + count + '" style="width:97%;  border: 3px solid darkgrey; position:absolute; margin-bottom:12px;"><table class="table myTable" style=\"width:100%\">\r\n  '+ appendcode +'  <\/table></div>');
            $('#t' + count + '').css("right", "500px");
            $('#t' + count + '').css("opacity", "0.1");
            $('#t' + count + '').animate({ right: '10px', height: tableHeight - 14, opacity: '1' }, 500);
            $('#t' + count + '').css('margin-top', '10px');
            tableElements = '';
        }
        
        count++;
        
    }

    return factory;
});