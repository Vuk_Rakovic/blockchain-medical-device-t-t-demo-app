﻿angular.module('NT.App')
.controller('HeaderCtrl', function HeaderCtrl($scope, $timeout) {
    $scope.tickInterval = 1000;

    $scope.test = "testing";

    var tick = function () {
        $scope.clock = Date.now() // get the current time
        $timeout(tick, $scope.tickInterval); // reset the timer
    }

    $timeout(tick, $scope.tickInterval);

});
