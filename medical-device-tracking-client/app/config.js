﻿'use strict'

angular.module('NT.Config', [])

//client auth seetings
.constant('AUTH_SETTINGS', {
    client_secret: '6EDD2EEC-1C23-4E3F-912C-AAA64771DFE7',
    client_id: 'Laetus.NT.Core.Platform.HMI',
    storage_key: 'authData',
    useAuth: false,
    refresh_token_after_last_activity_interval: 3000
})

.constant('AUTH_EVENTS', {
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized'
})

//should output debug information to browser console
.constant('DEBUG', true)
.constant('MAX_NUMBER_OF_SYS_MESSSAGES', 25)//maximum number of messages that app keeps in memory

//cookie settings
.constant('CURRENT_PAGE_INFO', 'current_page')//stores information about last browsed page
.constant('START_PAGE', 'home')//start page id

.constant('MANUALAGGREGATION_DEACTIVATION_INTERVAL', 10000)//number of bags to fill the pallet

//HMI client events
.constant('LOGGED_USER_EVENT', "LoggedUser")
.constant('LOCALE_CHANGED', 'locales_changed')
.constant('HMI_AGENT_OPERATIONAL', "HmiAgentOperational")//event fired when HMI backend went to operational state
.constant('TABLE_ADD_COLUMN_EVENT', 'Table_AddColumnDescription')//event fired by tableColumn (partial) component
.constant('TABLE_FULLY_COMPILED_EVENT', 'TableFullyCompiled')//event fired by table to
.constant('MODESELECTBUTTON_CHANGEMODE', 'changeMode')//fired when changing between modes (HMI/LineHMI)
.constant('FORM_ADD_FIELD_EVENT', 'Form_AddField')//event fired by component to notify Form to render it
.constant('TOPOLOGYBAR_ADD_CHILD_BUTTON_EVENT', 'TopologyBarLineButtonGroup_addChildButtonLink')//event fired by TopologyBarButton component to notify TopologyBarLine to render it
.constant('DEVICEVIEW_HAS_CHILDREN_EVENT', 'DeviceViewButtonGroup_hasChildren')//event fired by DeviceViewButtonGroup component to notify DeviceView to render it
.constant('CLEAR_MESSAGES_RESET', 'Clear_Messages_Reset')//event fired by DeviceViewButtonGroup component to notify DeviceView to render it
.constant('SUBMIT_INPUT', 'Submit_Input')

//Graphics directives events
.constant('FORCE_RENDER_SVG_EVENT', 'ForceRenderingOfSvgGraphics')//event fired when rerender of svg directives should be performed

//Logger setup object
.constant('LOGLEVEL', { verbose: 0, trace: 1, debug: 2, info: 3, warning: 4, error: 5})//Level of the log messages

.constant('PAGE_HEIGHT_OFFSET', 240)
.constant('CONTAINER_HEIGHT_OFFSET', 350)
;