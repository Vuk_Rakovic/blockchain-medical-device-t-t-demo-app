﻿(function() {
    'use strict';

    angular
        .module('NT.Directives',
        [])
        .directive('contentRenderer', function() {
            return {
                restrict: 'EA',
                replace: true,
                templateUrl: "app/components/content-renderer/content-renderer.html",
                transclude: false,
                scope: {
                    template: "@template"
                },
                controller: 'ContentRendererCtrl as vm'
            };
        })
        .directive('actions', function() {
            return {
                restrict: 'EA',
                replace: true,
                templateUrl: "app/components/actions/actions.html",
                transclude: false,
                scope: {
                
                },
                controller: 'ActionsCtrl as vm'
            };
        })
        .directive('modalConfirm', function() {
            return {
                restrict: 'EA',
                replace: true,
                templateUrl: "app/components/modals/modal-confirm/modal-confirm.html",
                transclude: false,
                scope: {
            
                },
                controller: 'ModalConfirmCtrl as vm'
            };
        });
}());