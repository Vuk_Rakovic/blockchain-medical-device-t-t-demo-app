﻿angular

.module('NT.App', [
    
    'ui.router', 'ui.bootstrap', 'ngAnimate', 
    'ngDialog',
    'ngCookies',
    'NT.Config',
    'NT.UIContainers',
    'NT.UIComponents',
    'NT.Directives',
    'NT.Auth',
    'NT.Localizations',
    'NT.AlarmEvents',
    'NT.Hub',
    'NT.Mappings',
    'NT.ActivePage',
    'NT.GraphicDirectives',
    'NT.DynamicCompilation',
    'NT.PageTypes',
    'NT.Dialog'
])

.value('$', $)

.factory('authHttpResponseInterceptor', function ($q, $location, $window, AUTH_SETTINGS) {
    return {
        response: function (response) {
            if (response.status === 401 || response.status === 400)
                console.log("Response Error " + response.status + "!", response);

            return response || $q.when(response);
        },
        responseError: function (rejection) {
            if (rejection.status == 401 || rejection.status == 400) {
                console.log("Response Error " + rejection.status + "!", rejection);
                $window.localStorage.removeItem(AUTH_SETTINGS.storage_key);
                $location.path('/login');
            }

            return $q.reject(rejection);
        }
    }
})

.config(['$stateProvider', '$urlRouterProvider', 'ngDialogProvider', '$httpProvider', 'AUTH_SETTINGS',
    function ($stateProvider, $urlRouterProvider, $ngDialogProvider, $httpProvider, AUTH_SETTINGS) {
    $urlRouterProvider.otherwise('home'); //Model must have page called START_PAGE

    //defining routes
    //$stateProvider
    //    .state('login', {
    //        url: "/login",
    //        templateUrl: "app/PageTypes/Login/Login.html",
    //        controller: 'LoginCtrl'
    //    })
    //    .state('content', {
    //        url: "/content/home",
    //        templateUrl: "app/components/home/home.html",
    //        controller: 'HomeCtrl as vm'
    //    });

    //var homeState = {
    //                        name: 'home',
    //                        url: "/home",
    //                        templateUrl: "app/components/home/home.html",
    //                        controller: 'HomeCtrl as vm'
    //                    };

    //                    var devChars = {
    //                        name: 'characteristics',
    //                        url: "/characteristics",
    //                        templateUrl: "app/components/home/characteristics.html",
    //                        controller: 'HomeCtrl as vm'
    //                    };

    //                    var devHistory = {
    //                        name: 'history',
    //                        url: "/history",
    //                        templateUrl: "app/components/home/history.html",
    //                        controller: 'HomeCtrl as vm'
    //                    };

    //                    var mappingEditorState = {
    //                        name: 'mappingEditor',
    //                        url: "/mapping-editor",
    //                        templateUrl: "app/components/mapping-editor/mapping-editor.html",
    //                        controller: 'MappingEditorCtrl as vm'
    //                    };

    //                    var mappingNewState = {
    //                        name: 'mappingNew',
    //                        url: "/mapping-new",
    //                        templateUrl: "app/components/mapping-new/mapping-new.html",
    //                        controller: 'MappingNewCtrl as vm'
    //                    };

    //                    var mappingModifyState = {
    //                        name: 'mappingModify',
    //                        url: "/mapping-modify?deviceReflectorName&mappingReference",
    //                        templateUrl: "app/components/mapping-modify/mapping-modify.html",
    //                        controller: 'MappingModifyCtrl as vm'
    //                    };

    //                    $stateProvider
    //                        .state(homeState)
    //                        .state(devChars)
    //                        .state(devHistory)
    //                        .state(mappingEditorState)
    //                        .state(mappingNewState)
    //                        .state(mappingModifyState)
    //                        .state('cmanager', {
    //                            url: "/cmanager",
    //                            templateUrl: "app/components/static/cmanager/cmanager.html",
    //                            controller: 'cmanagerCtrl'
    //                        }).state('modify', {
    //                            url: "/modify",
    //                            templateUrl: "app/components/static/modify/modify.html",
    //                            controller: 'modifyCtrl'
    //                        }).state('drawerforms', {
    //                            url: "/drawerforms",
    //                            templateUrl: "app/components/static/drawerforms/drawerforms.html",
    //                            controller: 'drawerformsCtrl'
    //                        }).state('modals', {
    //                            url: "/modals",
    //                            templateUrl: "app/components/static/modals/modals.html",
    //                            controller: 'ModalsCtrl'
    //                        }).state('buttons', {
    //                            url: "/buttons",
    //                            templateUrl: "app/components/static/buttons/buttons.html",
    //                            controller: 'buttonsCtrl'
    //                        });

    //setting up ngDialog defaults
    $ngDialogProvider.setDefaults({
        className: 'ngdialog-theme-default',
        plain: false,
        showClose: true,
        closeByDocument: false,
        closeByEscape: true,
        appendTo: false
    });

    if (AUTH_SETTINGS.useAuth)
        $httpProvider.interceptors.push('authHttpResponseInterceptor');
}])

.run(['$rootScope', '$templateCache', '$cookies', 'hubService', 'ConsoleLogger', 'authService', 'localizationService', 'navigationService', 'HMI_AGENT_OPERATIONAL',
     'eventAlarmsService', 'CURRENT_PAGE_INFO', 'AUTH_SETTINGS', 'PAGE_HEIGHT_OFFSET', 'CONTAINER_HEIGHT_OFFSET', '$window', 'START_PAGE',
function ($rootScope, $templateCache, $cookies, hubService, ConsoleLogger, authService, localizationService, navigationService, HMI_AGENT_OPERATIONAL,
     eventAlarmsService, CURRENT_PAGE_INFO, AUTH_SETTINGS, PAGE_HEIGHT_OFFSET, CONTAINER_HEIGHT_OFFSET, $window, START_PAGE) {

    //initialize global objects/constants
    $rootScope.__formResponses = {};
    $rootScope.activePage = {};

    //setup safeApply
    $templateCache.removeAll();
    $rootScope.safeApply = function (scope, callback) {
        if (scope.$$phase != '$apply' && scope.$$phase != '$digest' &&
            (!scope.$root || (scope.$root.$$phase != '$apply' && scope.$root.$$phase != '$digest'))) {
            scope.$apply();
        }

        if (angular.isFunction(callback))
            callback();
    };

    authService.restoreAuthData();
    localizationService.initialize();
    eventAlarmsService.initialize();
    navigationService.initialize();

    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        $rootScope._activeNodeId = toParams.contentId ? toParams.contentId : START_PAGE;//currently active node id that is accessible from all controllers
        hubService.removeTopicHandlers();
    });

    if (AUTH_SETTINGS.useAuth) {
        alert('sdf');
        hubService.initialize();
        navigationService.goToPage(START_PAGE);
    }
    else {
        if (authService.currentUser.isAuthenticated) {//user is already logged
            hubService.initialize();//creates signalR hub and starts it

            var lastActivePageInfo = $cookies.get(CURRENT_PAGE_INFO);
            if (lastActivePageInfo) {
                $rootScope.activePage = JSON.parse(lastActivePageInfo);
                navigationService.goToPage($rootScope.activePage._activeNodeId)//already logged in so go to last browsed page
            }
            else navigationService.goToPage(START_PAGE);
        }
        else
            authService.logOut();
    }

    var w = angular.element($window);

    $rootScope.getHeight = function (heightOffset) {
        if (window.self !== window.top) 
            return heightOffset - 120;

        return heightOffset;
    }

    $rootScope.calculateheight = function (element, heightOffset) {
        $(element).height(w.innerHeight() - heightOffset);
    }

    $(window).bind('resize', function () {
        $rootScope.calculateheight('.content-main', $rootScope.getHeight(PAGE_HEIGHT_OFFSET));
        $rootScope.calculateheight('.items-container', $rootScope.getHeight(CONTAINER_HEIGHT_OFFSET));
    });

    $rootScope.calculateheight('.content-main', $rootScope.getHeight(PAGE_HEIGHT_OFFSET));
    $rootScope.calculateheight('.items-container', $rootScope.getHeight(CONTAINER_HEIGHT_OFFSET));
}]);