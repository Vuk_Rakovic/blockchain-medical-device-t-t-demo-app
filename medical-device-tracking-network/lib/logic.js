/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
     * Device transport
     * @param {org.acme.mynetwork.DeviceTransport} deviceTransport - the DeviceTransport transaction
     * @transaction
     */

    function deviceTransport(tx) {

        tx.device.location = tx.newLocation;
        tx.device.locationName = tx.newLocationName;        
        tx.device.deviceState = tx.deviceState;

        if(tx.device.deviceTransactions == null)
          tx.device.deviceTransactions = [];
        tx.device.deviceTransactions.push(tx);

        return getAssetRegistry ('org.acme.mynetwork.Device')
                  .then(function (assetRegistry) {
                    return assetRegistry.update(tx.device)    		
                })       
        }
    
    /**
         * Device rejection
         * @param {org.acme.mynetwork.DeviceRejection} deviceRejection - the DeviceRejection transaction
         * @transaction
         */
    
    function deviceRejection(tx) {

        
        if(tx.restartLocation == null)
          tx.device.deviceStatus = 'REJECTED';
        else {
          tx.device.location = tx.restartLocation;
          tx.device.locationName = tx.restartLocation.name;
        }
        tx.device.deviceState = tx.deviceState;

        if(tx.device.deviceTransactions == null)
          tx.device.deviceTransactions = [];
        tx.device.deviceTransactions.push(tx);

          return getAssetRegistry ('org.acme.mynetwork.Device')
                  .then(function (assetRegistry) {
                    return assetRegistry.update(tx.device)    		
        })
    }
